package com.techyourchance.unittestingfundamentals.exercise1

import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.hamcrest.CoreMatchers.`is` as Is

class NegativeNumberValidatorTest {
    var SUT = NegativeNumberValidator()

    @Before
    fun initialize() {
        SUT = NegativeNumberValidator()
    }

    @Test
    fun isNegative_negative_true() {
        val result = SUT.isNegative(-4)
        assertThat(result, Is (true))
    }

    @Test
    fun isNegative_zero_false() {
        val result = SUT.isNegative(0)
        assertThat(result, Is (false))
    }

    @Test
    fun isNegative_positive_false() {
        val result = SUT.isNegative(7)
        assertThat(result, Is(false))
    }
}