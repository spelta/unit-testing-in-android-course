package com.techyourchance.unittestingfundamentals.example3

import org.junit.Assert.assertThat
import org.junit.Test
import org.hamcrest.CoreMatchers.`is` as Is

class IntervalTest {
    var SUT: Interval = Interval(-2, 8)

    @Test
    fun interval_starts_at_minus2_true() {
        val result = SUT.start
//        assertNotEquals( "start is not -1", result, -1 )
        assertThat(result, Is(-2))
    }

    @Test
    fun interval_ends_at_8_true() {
        val result = SUT.end
        assertThat(result, Is(8))
    }
}