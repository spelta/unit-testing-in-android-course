package com.techyourchance.unittestingfundamentals.example2

import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.hamcrest.CoreMatchers.`is` as Is

class StringReverserTest {

    private var SUT: StringReverser = StringReverser()

    @Before
    fun setup() {
        SUT = StringReverser()
    }

    @Test
    fun reverse_emptyString_emptyStringReturned() {
        val result = SUT.reverse("")
        assertThat(result, Is(""))
    }

    @Test
    fun reverse_singleCharacter_sameStringReturned() {
        val result = SUT.reverse("a")
        assertThat(result, Is("a"))
    }

    @Test
    @Throws(Exception::class)
    fun reverse_longString_reversedStringReturned() {
        val result = SUT.reverse("Vasiliy Zukanov")
        assertThat(result, Is("vonakuZ yilisaV"))
    }
}